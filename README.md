# ClubRobotEsisar v2
On V1, the site worked on a (sluggish) custom bad micro framework.  
With V2, I restarted from the ground up the backend, keeping some classes and the frontend.  
We use now Slim Framework for the routing, and Twig for the template engine. With it, we have a really great separation between Controllers and Views.  
We still do not have a MVC project, but it is starting to do, we're using models for reunions and articles.  

And we dropped file manager.
## Working
+ Home page (need to redo history gestion)
+ Admin page
  + Account gestion
+ Middlewares
+ Auth
+ Router
+ Controllers
+ Reunion gestion (full Ajax)
  + Subjects to discuss in the reunion, calendar scheduling
  + Summary
+ Dark/Light theme

## Not working/TODO
+ Inventory
+ Requests + linking with inventory
+ Projects gestion with calendar, tasks assignment and todo list
+ Notification

## Libraries used
+ Bootstrap (CSS/JS)
  + Nunito font
  + FontAwesome
  + Animate.css
  + Bootstrap table (https://bootstrap-table.com/docs/getting-started/introduction/)
  + Awesome bootstrap checkbox (https://github.com/flatlogic/awesome-bootstrap-checkbox)
  + Datepicker (https://github.com/fengyuanchen/datepicker)
  + Bootstrap Notify (http://bootstrap-notify.remabledesigns.com/)
  + Bootstrap Confirmation
  + CookieJS
+ jQuery
+ Slim framework
+ Twig
+ Parsedown (Markdown parser)

## Install
Clone repo, and run `composer install` (Get [composer here](https://getcomposer.org/)).  
Then, run `php install.php` ; it'll create the `config.ini` that you will need to populate with your SQL credentials.  
When it's done, re-run `php install.php`, it'll create tables, rest of config files, and a `cre-default.conf`. This is an Apache Virtual Host config file, which will work out-of-the-box (hopefully).  
Copy it in `/etc/apache2/sites-available`, and run `a2ensite cre-default`, and reload Apache by running `systemctl reload apache2`.  
For this to work, you'll need Apache php module, and rewrite module.  
Don't forget permissions on directory (recursively) : `www-data` owner, with chmod 701.  
Then, you can access the site at [http://localhost:8888](http://localhost:8888).

## Changelog
### 19-21/06
First commits, redone backend, and a lot of things

### 23/06
Finished(-ish) /reunions by adding markdown parser  
Redid middleware system, need polishing