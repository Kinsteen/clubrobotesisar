<?php

class App{

    static $db = null;

    static function getDatabase() : Database {
        if(!self::$db) {
            $ini = parse_ini_file('../config.ini');
            self::$db = Database::get(array(
                'type' => $ini['DB_TYPE'],
                'host' => $ini['DB_HOST'],
                'name' => $ini['DB_NAME'],
                'user' => $ini['DB_USER'],
                'pass' => $ini['DB_PASS']
            ));
        }
        
        return self::$db;
    }

    static function setMode($mode) {
        file_put_contents('../app.mode', $mode);
    }

    static function mode() {
        return file_get_contents('../app.mode');
    }

    static function addHistory(Database $db, string $user, string $category, array $data) {
        $data = (object) $data;

        setlocale(LC_TIME, "fr_FR");
        $date = ucfirst(strftime("%A %e/%m/%Y, %Hh%M"));
        switch ($category) {
            case 'inscription':
                $insert = array('title' => 'Nouvelle inscription !', 'content' => $user.' s\'est incrit !', 'date' => $date);
                $db->insert("cre_history", $insert);
                break;
            case 'inventaire':
                $insert = array('title' => 'Nouveau objet dans l\'inventaire !', 'content' => "$user a ajouté $data->item dans l'inventaire.", 'date' => $date);
                $db->insert("cre_history", $insert);
                break;
            
            case 'demande_achat':
                $insert = array('user' => $user, 'category' => 'demande_achat', 'item' => $data->item, 'date' => $date);
                $db->insert("cre_history", $insert);
                break;
            
            case 'demande_pret':
                $insert = array('user' => $user, 'category' => 'demande_pret', 'item' => $data->item, 'date' => $date);
                $db->insert("cre_history", $insert);
                break;

            case 'demande_impression':
                $insert = array('user' => $user, 'category' => 'demande_impression', 'item' => $data->item, 'date' => $date);
                $db->insert("cre_history", $insert);
                break;

            case 'reunions_ghost':
                $h = $db->select("SELECT * FROM cre_history WHERE content='".$user." a modifié le contenu de la prochaine réunion du ".$data->date.".'");
                if(count($h) == 0) {
                    $insert = array('title' => "Modification de la prochaine réunion", 'content' => "$user a modifié le contenu de la prochaine réunion du $data->date.", 'date' => $date);
                    $db->insert("cre_history", $insert);
                }
                break;

            default:
                exit();
                break;
        }
    }

    static function addBreach($user, $action) {
        setlocale(LC_TIME, "fr_FR");
        $date = ucfirst(strftime("%A %e/%m/%Y %Hh%M"));
        $insert = array('user' => $user, 'action' => $action, 'date' => $date);
        App::getDatabase()->insert("cre_breach", $insert);
    }

    /**
     * Generates a token, save it in the session, and return it to be in page.
     * @return string Token
     * @throws Exception
     */
    static function generateCSRF() {
        $token = bin2hex(random_bytes(32));
        Session::getInstance()->write('csrf', $token);
        return $token;
    }

    static function errorReport(string $message) { // This is an error reporting tool, it'll write errors on a log file.
        setlocale(LC_TIME, "fr_FR");
        $current = App::getErrorReport();

        $current .= strftime('%d/%m/%y, %Hh%M:%S: ').$message."\n";
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/../error.log', $current);
    }

    static function getErrorReport() {
        return file_get_contents($_SERVER['DOCUMENT_ROOT'].'/../error.log');
    }
}
