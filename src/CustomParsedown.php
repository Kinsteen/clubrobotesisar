<?php

/**
 * Class CustomParsedown
 * This is to modify links, so it open a new tab when clicked
 */
class CustomParsedown extends Parsedown {
    protected function inlineLink($Excerpt) {
        $link = parent::inlineLink($Excerpt); // Get normal link

        if (!isset($link)) {
            return null;
        }

        $link['element']['attributes']['target'] = '_blank'; // adding attribute target

        return $link; // return the new link for the script to continue
    }
}