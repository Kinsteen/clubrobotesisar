<?php


namespace Middlewares;


use App;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use User;

class ModeMiddleware {

    /**
     * @var ContainerInterface
     */
    private $container;

    private $excluded;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->excluded = ['/admin', '/accounts/login', '/api/change-mode'];
    }

    public function __invoke(Request $request, Response $response, callable $next) {
        if(App::mode() == 'dev') {
            if(User::logged() == null || User::logged()->rank < 2) {
                if(!in_array($request->getUri()->getPath(), $this->excluded)) {
                    return $this->container->get('view')->render($response, 'error/dev.twig');
                }
            }
        } else if(App::mode() == 'maintenance') {
            if(User::logged() == null || User::logged()->rank < 2) {
                if(!in_array($request->getUri()->getPath(), $this->excluded)) {
                    return $this->container->get('view')->render($response, 'error/maintenance.twig');
                }
            }
        }

        return $next($request, $response);
    }

}