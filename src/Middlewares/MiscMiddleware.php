<?php

namespace Middlewares;

use Slim\Http\Request;
use Slim\Http\Response;
use User;

/**
 * This is the middleware checker class
 */
class MiscMiddleware extends Middleware {
    public function guest(Request $request, Response $response, callable $next) {
        if(User::logged()) {
            return $this->stopExecution($request, $response,'');
        }

        return $next($request, $response);
    }

    public function remember(Request $request, Response $response, callable $next) {
        if(!User::logged()) {
            if(isset($_COOKIE['user']) && User::remember($_COOKIE['user'])) {
                return $response->withRedirect($_SERVER['REQUEST_URI']);
            }
        }

        return $next($request, $response);
    }
}