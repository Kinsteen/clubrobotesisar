<?php


namespace Middlewares;


use Session;
use Slim\Http\Request;
use Slim\Http\Response;

class Middleware {

    protected function stopExecution(Request $request, Response $response, string $error, $redirect = '/') { // If it's an ajax request, just die and send error for the js to catch up
        if($request->getParam('req') == 'ajax') { // This will be set for every $.post call
            $cleanresponse = new Response();
            $cleanresponse->write('-10');
            return $cleanresponse->withHeader('Content-Type', 'text/plain');
        } else {
            Session::getInstance()->setFlash('danger', $error);
            return $response->withRedirect($redirect);
        }
    }

}