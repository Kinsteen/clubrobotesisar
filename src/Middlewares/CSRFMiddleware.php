<?php


namespace Middlewares;

use \App;
use \Session;
use \Slim\Http\Request;
use \Slim\Http\Response;
use \User;
use \Util;

class CSRFMiddleware extends Middleware {

    public function __invoke(Request $request, Response $response, callable $next) {
        if(!Util::compare($request->getParam('csrf'), Session::getInstance()->read('csrf'))) {
            if(User::logged() != null) {
                App::addBreach(
                    User::logged()->name,
                    'CSRF check failed ! Expected '. Session::getInstance()->read('csrf').', got '.$_POST['csrf'].'. From '.$_SERVER['REQUEST_URI']
                );
                User::logged()->logout(); // Logs out user, because of csrf breach
            } else {
                App::addBreach('null', 'CSRF check failed ! Expected '. Session::getInstance()->read('csrf').', got '.$_POST['csrf'].'. From '.$_SERVER['REQUEST_URI']);
            }

            $mode = Session::getInstance()->read('mode');
            if($mode == 'dev' || $mode == 'maintenance') { // Redirect to login if dev or maintenance mode
                return $this->stopExecution($request, $response, '/accounts/login');
            }

            return $this->stopExecution($request, $response, 'Un problème d\'authentification est survenu, veuillez vous reconnecter.');
        }

        return $next($request, $response); // Continue
    }

}