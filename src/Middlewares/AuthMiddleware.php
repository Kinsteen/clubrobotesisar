<?php


namespace Middlewares;


use App;
use Slim\Http\Request;
use Slim\Http\Response;
use \User;

class AuthMiddleware extends Middleware {
    private $rank;

    public function __construct(int $rank = 0) {
        $this->rank = $rank;
    }

    public function __invoke(Request $request, Response $response, callable $next) {
        if(User::logged()) { // Check if user is logged
            if(User::logged()->rank < $this->rank) { // Check if he has sufficient rank
                $mode = App::mode();
                if($mode == 'dev' || $mode == 'maintenance') { // Redirect to login if dev or maintenance mode
                    return $this->stopExecution($request, $response, 'Vous n\'avez pas les permissions pour accéder à cette page.', '/accounts/login');
                }
                
                return $this->stopExecution($request, $response, 'Vous n\'avez pas les permissions pour accéder à cette page.'); // Stops.
            }
        } else { // If not, redirect him to home
            $mode = App::mode();
            if($mode == 'dev' || $mode == 'maintenance') { // Redirect to login if dev or maintenance mode
                return $this->stopExecution($request, $response, 'Vous n\'avez pas les permissions pour accéder à cette page.', '/accounts/login');
            }
            
            return $this->stopExecution($request, $response, 'Vous devez être connecté pour accéder à cette page.');
        }


        return $next($request, $response);
    }

}