<?php

use \CustomParsedown as Parsedown;

class Articles
{
    private $db;

    public function __construct(Database $db) {
        $this->db = $db;
    }

    public function all() {
        $parser = new Parsedown();

        $res = $this->db->select('SELECT * FROM cre_articles');

        foreach ($res as $key => $value) {
            $authorname = $this->db->select('SELECT * FROM cre_users WHERE id=:id', array('id' => $value->author))[0]->name;
            $value->author = $authorname;
            $value->content = $parser->setMarkupEscaped(true)->text($value->content);
        }

        return $res;
    }
}