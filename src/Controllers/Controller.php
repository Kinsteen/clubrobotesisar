<?php


namespace Controllers;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;

class Controller
{
    protected $container = null;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function render(Response $response, string $view, array $args = []) {
        return $this->container->get('view')->render($response, $view, $args);
    }
}