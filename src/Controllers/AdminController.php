<?php

namespace Controllers;

use App;
use Slim\Http\Request;
use Slim\Http\Response;
use User;

class AdminController extends Controller {
    public function index(Request $request, Response $response, array $args) {
        $db = App::getDatabase();
        $users = $db->select('SELECT id, name, mail, rank FROM cre_users');
        $breaches = $db->select('SELECT * FROM cre_breach ORDER BY id DESC');

        return $this->render($response, 'admin/index.twig', array('users' => $users, 'mode' => App::mode(), 'errorreport' => App::getErrorReport(), 'breaches' => $breaches));
    }

    public function rank(Request $request, Response $response) {
        $db = App::getDatabase();
        $response = new Response();
        /*
         * So ! user = logged user who does the action
         * target = user targeted by the rank change
         * we need to authenticate very well the user, because if someone can rank up even if he doesn't have the right... lul
         */

        $user = User::logged();
        $target = new User($request->getParam('account'));
        $targetrank = $request->getParam('rank');
        $action = $request->getParam('action');

        if ($target->rank == $targetrank) {
            if ($action == "up") {
                if ($user->rank > $targetrank + 1) { // Ex : user is council -> can up member to active but can't up to council
                    $db->update('cre_users', array('rank' => $targetrank + 1), array('id' => $target->id));
                    return $response->write('1');
                } else {
                    return $response->write('0');
                }
            } else if ($action == "down") {
                if ($user->rank > $targetrank) { // Ex : user is council -> can up member to active but can't up to council
                    if($targetrank-1 >= 0) {
                        $db->update('cre_users', array('rank' => $targetrank - 1), array('id' => $target->id));
                        return $response->write('1');
                    } else {
                        return $response->write('-1');
                    }
                } else {
                    return $response->write('0');
                }
            } else {
                $user->logout();
                App::addBreach($user->name, 'Used different action on rank up/down');
                return $response->write('-1');
            }
        } else {
            $user->logout();
            App::addBreach($user->name, 'Tried to falsify rank on rank up/down');
            return $response->write('-1');
        }
    }
}
