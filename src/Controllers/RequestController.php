<?php
namespace Controllers;

use \App as App;
use \Session as Session;
use \User as User;

class RequestController {
    public function showBuy() {
        App::view('request/buy');
    }

    public function showRent() {
        App::view('request/rent');
    }

    public function showPrint() {
        App::view('request/print');
    }
}