<?php
namespace Controllers;

use App ;
use Slim\Http\Request;
use Slim\Http\Response;
use User;

class APIController extends Controller {
    public function rank() {
        $db = App::getDatabase();

        $usertorank = $db->select('SELECT rank FROM cre_users WHERE name=:name', ['name' => $_POST['account']])[0];

        if(User::logged()->rank >= 2 && $_POST['rank'] <= 1 && $usertorank->rank <= 1 ) {
            $where = array('name' => $_POST['account']);
            $dataArray = array('rank' => $_POST['rank']);
            $db->update('cre_users', $dataArray, $where);
            exit('1');
        } else {
            App::addBreach(User::logged()->name, 'Ranking failed due to insufficient rank. Impersonator rank : '.User::logged()->name.'. Tried to rank up/down : '.$_POST['account'].', with rank '.$usertorank->rank);
            User::logged()->logout();
            exit('0');
        }
    }

    public function clearHistory() {
        $db = App::getDatabase();

        $where = array('mail' => User::logged()->mail);
        $dataArray = array('seen' => "");
        $db->update('cre_users', $dataArray, $where);
        die('1');
    }

    public function updateHistory() {
        $db = App::getDatabase();

        $params = array('mail' => User::logged()->mail);
        $result = $db->select('SELECT seen FROM cre_users WHERE mail=:mail', $params);
        $history = $result[0]->seen;

        if($history == "") {
            $new_history = $_POST['id'];
        } else {
            $new_history = $history . "," . $_POST['id'];
        }
        
        $where = array('mail' => User::logged()->mail);
        $dataArray = array('seen' => $new_history);
        $db->update('cre_users', $dataArray, $where);
    }

    public function changeMode() {
        if($_POST['mode'] == 'maintenance') {
            if(App::mode() == "maintenance") {
                App::setMode('prod');
                echo 'prod';
            } else {
                App::setMode('maintenance');
                echo "maintenance";
            }
        } else if($_POST['mode'] == 'dev') {
            if(App::mode() == "dev") {
                App::setMode('prod');
                echo "prod";
            } else {
                App::setMode('dev');
                echo "dev";
            }
        }
    }

    public function changeTheme() {
        $db = App::getDatabase();

        $db->update('cre_users', array('theme' => $_POST['theme']), array('mail' => User::logged()->mail));
    }
}