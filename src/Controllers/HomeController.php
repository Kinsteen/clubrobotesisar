<?php


namespace Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use User;
use App;

class HomeController extends Controller {

    public function home(Request $request, Response $response) {
        $user = User::logged();
        $logged = false;
        $db = App::getDatabase();
        $cards = [];

        if($user) {
            $logged = true;
            $params = array('mail' => $user->mail);
            $result = $db->select('SELECT seen FROM cre_users WHERE mail=:mail', $params);
            $logs = array_reverse($db->select('SELECT * FROM cre_history'));
            $history = explode(',', $result[0]->seen);

            $counter = 0;
            foreach($logs as $key => $log) {
                if(!in_array($log->id, $history)) {
                    $counter++;
                    array_push($cards, (object) array('id' => $log->id, 'title' => $log->title, 'content' => $log->content, 'date' => $log->date));

                    if($counter == 6) {
                        break;
                    }
                }
            }
        } else {
            $counter = 0;
        }

        return $this->render($response, 'home.twig', [
            'cards' => $cards,
            'counter' => $counter
        ]);
    }

    public function links(Request $request, Response $response): Response {
        return $this->render($response, 'links.twig');
    }
}