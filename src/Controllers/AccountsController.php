<?php
namespace Controllers;

use \App as App;
use RestCord\DiscordClient;
use \Session as Session;
use Slim\Http\Request;
use Slim\Http\Response;
use \User as User;

class AccountsController extends Controller {
    public function index(Request $request, Response $response) {
        return $this->render($response, 'accounts/index.twig');
    }

    public function showLogin(Request $request, Response $response) {
        $name = Session::getInstance()->read('name'); // Save name if failed login
        Session::getInstance()->unset('name');
        return $this->render($response, 'accounts/login.twig', ['name' => $name]);
    }

    public function showRegister(Request $request, Response $response) {
        $name = Session::getInstance()->read('name'); // Same as login
        $mail = Session::getInstance()->read('mail');
        Session::getInstance()->unset('name');
        Session::getInstance()->unset('mail');
        return $this->render($response, 'accounts/register.twig', ['name' => $name, 'mail' => $mail]);
    }

    public function login(Request $request, Response $response) {
        Session::getInstance()->write('name', $_POST['mail']);
        if(!empty($_POST['mail']) && !empty($_POST['password'])) {
            $user = new User($_POST['mail']);
            if($user->login($_POST['mail'], $_POST['password'], $_POST['remem'])) {
                Session::getInstance()->unset('name');
                Session::getInstance()->setFlash('success', 'Vous êtes bien connecté.');
                if(isset($_GET['page'])) { // If page is specified, redirect to it
                    return $response->withRedirect($_GET['page']);
                } else {
                    return $response->withRedirect('/');
                }
            } else {
                Session::getInstance()->setFlash('danger', 'Mot de passe ou adresse mail incorrects.');
                return $response->withRedirect('/accounts/login');
            }
        } else {
            Session::getInstance()->setFlash('danger', "Vous devez remplir tous les champs.");
            return $response->withRedirect('/accounts/login');
        }
    }

    public function register(Request $request, Response $response) {
        $db = App::getDatabase();
        Session::getInstance()->write('name', $_POST['name']);
        Session::getInstance()->write('mail', $_POST['mail']);

        if(!empty($_POST['mail']) && !empty($_POST['password']) && !empty($_POST['confirm-password'])) {
            if(!empty($_POST['name'])) {
                if(filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
                    if($_POST['password'] == $_POST['confirm-password']) {
                        if(strlen($_POST['password']) >= 6) {
                            $count = $db->count("SELECT count(mail) FROM cre_users WHERE mail=:mail", array('mail' => $_POST['mail']));
                            if($count == "0") {
                                $count2 = $db->count("SELECT count(name) FROM cre_users WHERE name=:name", array('name' => $_POST['name']));
                                if($count2 == "0") {
                                    Session::getInstance()->unset('name');
                                    Session::getInstance()->unset('mail');

                                    User::register(htmlspecialchars($_POST['name']), $_POST['mail'], $_POST['password']);
                                    App::addHistory($db, htmlspecialchars($_POST['name']), 'inscription', []);
                                    Session::getInstance()->setFlash('success', "Vous êtes bien inscrit !");
                                    return $response->withRedirect('/');
                                } else {
                                    Session::getInstance()->setFlash('danger', "Le pseudo est déjà enregistré.");
                                    return $response->withRedirect('/accounts/register');
                                }
                            } else {
                                Session::getInstance()->setFlash('danger', "L'adresse email est déjà enregistrée.");
                                return $response->withRedirect('/accounts/register');
                            }
                        } else {
                            Session::getInstance()->setFlash('danger', "Le mot de passe est trop court.");
                            return $response->withRedirect('/accounts/register');
                        }
                    } else {
                        Session::getInstance()->setFlash('danger', "Les mots de passe ne correspondent pas.");
                        return $response->withRedirect('/accounts/register');
                    }
                } else {
                    Session::getInstance()->setFlash('danger', "L'adresse email n'est pas valide.");
                    return $response->withRedirect('/accounts/register');
                }
            } else {
                Session::getInstance()->setFlash('danger', "Le pseudo ne peut pas être vide.");
                return $response->withRedirect('/accounts/register');
            }
        } else {
            Session::getInstance()->setFlash('danger', "Vous devez remplir tous les champs.");
            return $response->withRedirect('/accounts/register');
        }
    }

    public function logout(Request $request, Response $response) {
        User::logged()->logout();
        Session::getInstance()->setFlash('success', 'Vous êtes maintenant déconnecté.');
        return $response->withRedirect('/');
    }

    public function sendMailRecovery(Request $request, Response $response) {
        $db = App::getDatabase();
        if(!empty($_POST['mail'])) {
            if(filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
                $count = $db->count("SELECT count(mail) FROM cre_users WHERE mail=:mail", array('mail' => $_POST['mail']));
                if($count == "1") {
                    /** @noinspection PhpUnhandledExceptionInspection */
                    $token = bin2hex(random_bytes(50));
                    $headers  = "From: Club Robot Esisar < me@kinsteen.fr >\n";
                    $headers .= "X-Sender: Club Robot Esisar < me@kinsteen.fr >\n";
                    $headers .= 'X-Mailer: PHP/' . phpversion()."\n";
                    $headers .= "Return-Path: me@kinsteen.fr\n"; // Return path for errors
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=utf8-bin\n";

                    mail($_POST['mail'], '[Site Club Robot Esisar] Mot de passe oublié', 'Veuillez aller sur ce lien pour définir un nouveau mot de passe.<br><a href="https://cre.kinsteen.fr/accounts/password-recovery?token='.$token.'&mail='.$_POST['mail'].'">Cliquez ici</a>', $headers);

                    $db->update('cre_users', array('password' => '', 'change_password' => $token), array('mail' => $_POST['mail']));
                }
            }

            Session::getInstance()->setFlash('success', 'Un lien de récupération a été envoyé.');
        } else {
            Session::getInstance()->setFlash('danger', 'Veuillez remplir tous les champs.');
        }

        return $this->render($response, 'accounts/lostPassword.twig');
    }

    public function showRecover() {
        if(isset($_GET['mail']) && isset($_GET['token'])) {
            $db = App::getDatabase();
            $count = $db->count("SELECT count(*) FROM cre_users WHERE mail=:mail AND change_password=:token", array('mail' => $_GET['mail'], 'token' => $_GET['token']));
            if($count == '1') {
                App::view('accounts/changePassword');
            } else {
                App::view('accounts/errorPasswordRecovery');
            }
        } else {
            App::redirect('/');
        }
    }

    public function submitPassword() {
        $db = App::getDatabase();
        if(!empty($_POST['pass1']) && !empty($_POST['pass2'])) {
            if($_POST['pass1'] == $_POST['pass2']) {
                if(strlen($_POST['pass1']) >= 6) {
                    $hashed = password_hash($_POST['pass1'], PASSWORD_ARGON2ID, array('memory_cost' => 4096,'time_cost' => 5, 'threads' => 8));
                    $db->update('cre_users', array('password' => $hashed, 'change_password' => ''), array('mail' => $_GET['mail']));
                    Session::getInstance()->setFlash('success', 'Votre mot de passe a été changé. Veuillez vous reconnecter.');
                    App::redirect('/');
                } else {
                    Session::getInstance()->setFlash('danger', 'Votre mot de passe est trop court. (au moins 6 caractères)');
                    $this->showRecover();
                }
            } else {
                Session::getInstance()->setFlash('danger', 'Les mots de passe ne correspondent pas.');
                $this->showRecover();
            }
        } else {
            $this->showRecover();
        }
    }

    public function changePassword() {
        $db = App::getDatabase();

        $result = $db->select('SELECT password FROM cre_users WHERE mail=:mail', array('mail' => User::logged()->mail))[0];

        if(!empty($_POST['currentPassword']) && !empty($_POST['newPassword']) && !empty($_POST['newPassword2'])) {
            $currentPassword = $_POST['currentPassword'];
            $newPassword = $_POST['newPassword'];
            $newPassword2 = $_POST['newPassword2'];

            if(password_verify($currentPassword, $result->password)) {
                if($newPassword == $newPassword2) {
                    if(strlen($newPassword) >= 6) {
                        // $auth->changePassword(User::logged()->mail, $newPassword);
                        $token = bin2hex(random_bytes(50));
                        $hashed = password_hash($newPassword, PASSWORD_ARGON2ID, array('memory_cost' => 4096,'time_cost' => 5, 'threads' => 8));

                        $headers  = "From: Club Robot Esisar < me@kinsteen.fr >\n";
                        $headers .= "X-Sender: Club Robot Esisar < me@kinsteen.fr >\n";
                        $headers .= 'X-Mailer: PHP/' . phpversion()."\n";
                        $headers .= "Return-Path: me@kinsteen.fr\n"; // Return path for errors
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=utf8-bin\n";

                        mail(User::logged()->mail, '[Site Club Robot Esisar] Changement de mot de passe', 'Veuillez aller sur ce lien pour confirmer votre changement de mot de passe. Si vous n\'êtes pas à l\'origine de cette action, veuillez changer votre mot de passe immédiatement.<br><a href="https://cre.kinsteen.fr/accounts/change-password?token='.$token.'&mail='.User::logged()->mail.'">Cliquez ici</a>', $headers);

                        $db->update('cre_users', array('change_password' => $token, 'temp_password' => $hashed), array('mail' => User::logged()->name));

                        User::logged()->logout();
                        Session::getInstance()->setFlash('success', 'Un mail a été envoyé pour changer votre mot de passe.');
                        App::redirect('/');
                    }
                } else {
                    Session::getInstance()->setFlash('danger', 'Les mots de passe ne correspondent pas.');
                    $this->index();
                }
            } else {
                Session::getInstance()->setFlash('danger', 'Votre mot de passe est invalide.');
                $this->index();
            }
        } else {
            $this->index();
        }
    }

    public function validatePasswordChange() {
        if(isset($_GET['mail']) && isset($_GET['token']) && !empty($_GET['token']) && !empty($_GET['mail'])) {
            $db = App::getDatabase();
            $count = $db->count("SELECT count(*) FROM cre_users WHERE mail=:mail AND change_password=:token", array('mail' => $_GET['mail'], 'token' => $_GET['token']));
            if($count == '1') {
                $q = $db->select('SELECT temp_password FROM cre_users WHERE mail=:mail', array('mail' => $_GET['mail']));
                $db->update('cre_users', array('password' => $q[0]->temp_password, 'change_password' => '', 'temp_password' => ''), array('mail' => $_GET['mail']));
                Session::getInstance()->setFlash('success', 'Votre mot de passe a bien été changé. Veuillez vous reconnecter.');
                App::redirect('/');
            } else {
                die('Une erreur est survenue. Veuillez recliquer sur le lien du mail. Si cela ne résoud pas le problème, veuillez contacter un administateur.');
            }
        } else {
            App::redirect('/');
        }
    }
}
