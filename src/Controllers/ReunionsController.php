<?php
namespace Controllers;

use \App as App;
use CustomParsedown as Parsedown;
use \Session as Session;
use Slim\Http\Request;
use Slim\Http\Response;
use \User as User;
use \Reunions as Reunions;

class ReunionsController extends Controller {
    /*
     * A model could be great to use here, but fuck it (update: working on it)
     * We'll use entry id=1 for the new "ghost" reunion, for orders and date
     * Then, when adding, we'll create a new entry, copying orders and content
     * and reset entry id=1 to have the next new reunion
     */

    public function index(Request $request, Response $response) {
        $db = App::getDatabase();
        $model = new Reunions($db);

        $ghost = $model->ghost();

        if($model->count()) { // There is at least one other reunion than the ghost
            $parser = new Parsedown();

            $last = $model->last();

            $contents = json_decode($last->content);

            foreach($contents as $key => $content) {
                $contents[$key] = $parser->setMarkupEscaped(true)->text($content);
            }

            return $this->render($response, 'reunion/index.twig',
                array(
                    'last' => array(
                        'date' => $last->date,
                        'orders' => json_decode($last->orders),
                        'contents' => $contents
                    ),
                    'ghost' => array(
                        'date' => $ghost->date,
                        'orders' => json_decode($ghost->orders)
                    )
                )
            );
        } else {
            return $this->render($response, 'reunion/index.twig',
                array(
                    'last' => array(
                        'date' => null,
                        'orders' => null,
                        'contents' => null
                    ),
                    'ghost' => array(
                        'date' => $ghost->date,
                        'orders' => json_decode($ghost->orders)
                    )
                )
            );
        }
    }

    public function editGhost(Request $request, Response $response) {
        $response = new Response();
        $date = $request->getParam('date');
        $orders = json_encode($request->getParam('orders'));

        $db = App::getDatabase();
        $model = new Reunions($db);
        $response->write($model->editGhost($date, $orders));

        App::addHistory($db, User::logged()->name, 'reunions_ghost', ['date' => $date]);

        return $response->withHeader('Content-Type', 'text/plain');
    }

    public function showEdit(Request $request, Response $response, array $args) {
        // This is to edit a certain report
        $id = $args['id'];

        $db = App::getDatabase();
        $res = $db->select('SELECT * FROM cre_reunions WHERE id=:id', array('id' => $id))[0];

        // retrieve everything from database from id
        return $this->render($response, 'reunion/edit.twig',
            array(
                'id' => $id,
                'date' => $res->date,
                'orders' => json_decode($res->orders),
                'contents' => json_decode($res->content)
            )
        );
    }

    public function postEdit(Request $request, Response $response) {
        $response = new Response();
        $change = $request->getParam('changedate');
        $date = $request->getParam('date');
        $orders = json_encode($request->getParam('orders'));
        $contents = json_encode($request->getParam('content'));

        $db = App::getDatabase();
        if($change == "true") {
            $olddate = $request->getParam('olddate');

            $q = $db->select('SELECT * FROM cre_reunions WHERE date=:date', array('date' => $date));
            if(sizeof($q) > 0) {
                $response->write('-1');
                return $response;
            }

            $q = $db->select('SELECT * FROM cre_reunions WHERE date=:olddate', array('olddate' => $olddate))[0];

            $db->update('cre_reunions', array('date' => $date, 'orders' => $orders, 'content' => $contents), array('id' => $q->id));
        } else {
            $db->update('cre_reunions', array('orders' => $orders, 'content' => $contents), array('date' => $date));
        }

        $response->write('1');

        return $response->withHeader('Content-Type', 'text/plain');
    }

    public function showList(Request $request, Response $response) {
        // Show every report, and select the one to be modified
        $db = App::getDatabase();
        $res = $db->select('SELECT id,date,orders FROM cre_reunions WHERE id != 1');

        foreach($res as $reu) { // Is normally string, need to convert one by one to a php object
            $reu->orders = json_decode($reu->orders);
        }

        usort($res, function ($a, $b) {
            return strtotime(str_replace('/', '-', $b->date)) - strtotime(str_replace('/', '-', $a->date));
        });

        return $this->render($response, 'reunion/list.twig', array('data' => $res));
    }

    public function show(Request $request, Response $response, array $args) {
        $id = $args['id'];
        $parser = new Parsedown();

        $db = App::getDatabase();
        $res = $db->select('SELECT * FROM cre_reunions WHERE id=:id', array('id' => $id));
        $contents = json_decode($res[0]->content);

        foreach($contents as $key => $content) {
            $contents[$key] = $parser->setMarkupEscaped(true)->text($content);
        }

        if(sizeof($res) == 1) {
            // retrieve everything from database from id
            return $this->render($response, 'reunion/show.twig',
                array(
                    'id' => $id,
                    'date' => $res[0]->date,
                    'orders' => json_decode($res[0]->orders),
                    'contents' => $contents
                )
            );
        } else {
            return $this->render($response, 'reunion/show.twig',array('id' => null));
        }
    }

    public function showAdd(Request $request, Response $response) {
        $db = App::getDatabase();
        $res = $db->select('SELECT * FROM cre_reunions WHERE id=1')[0];
        return $this->render($response, 'reunion/add.twig',
            array(
                'date' => $res->date,
                'orders' => json_decode($res->orders),
                'contents' => json_decode(htmlspecialchars_decode($res->content, ENT_QUOTES))
            ));
    }

    public function postAdd(Request $request, Response $response) {
        $response = new Response();
        $date = $request->getParam('date');
        $orders = json_encode($request->getParam('orders'));
        $content = json_encode($request->getParam('content'));

        $db = App::getDatabase();
        $q = $db->count('SELECT count(*) FROM cre_reunions WHERE date=:date AND id != 1', array('date' => $date));
        if($q == 0) {
            $db->insert('cre_reunions', array('date' => $date, 'orders' => $orders, 'content' => $content));
            $db->update('cre_reunions', array('date' => '', 'orders' => '', 'content' => ''), array('id' => 1));
            $response->write('1');
            return $response->withHeader('Content-Type', 'text/plain');
        } else {
            $db->update('cre_reunions', array('orders' => $orders, 'content' => $content), array('id' => 1));
            $response->write('-1');
            return $response->withHeader('Content-Type', 'text/plain');
        }
    }

    public function delete(Request $request, Response $response) {
        $response = new Response();
        $id = $request->getParam('id');

        $db = App::getDatabase();
        if($id != "1") {
            $q = $db->delete('cre_reunions', array('id' => $id));
            if($q == 1) {
                Session::getInstance()->setFlash('success', 'La réunion a bien été supprimée.');
                $response->write('1');
                return $response->withHeader('Content-Type', 'text/plain');
            } else {
                $response->write('-1');
                return $response->withHeader('Content-Type', 'text/plain');
            }
        } else {
            $response->write('-1');
            return $response->withHeader('Content-Type', 'text/plain');
        }
    }
}