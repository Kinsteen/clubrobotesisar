<?php

namespace Controllers;

use \App;
use CustomParsedown as Parsedown;
use \Session;
use Slim\Http\Request;
use Slim\Http\Response;
use \User;
use \Articles;

class ArticlesController extends Controller
{
    public function index(Request $request, Response $response) {
        $parser = new Parsedown();
        $model = new Articles(App::getDatabase());
        return $this->render($response, 'articles/index.twig', array('articles' => $model->all()));
    }
}