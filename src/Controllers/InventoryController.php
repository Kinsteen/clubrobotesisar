<?php

namespace Controllers;

use App;
use Slim\Http\Request;
use Slim\Http\Response;
use User;

class InventoryController extends Controller
{
    public function index(Request $request, Response $response, array $args) {
        return $this->render($response, 'inventory.twig');
    }
}