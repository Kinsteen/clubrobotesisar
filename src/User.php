<?php

/**
 * User Class
 */
class User
{
    private $db;

    private $exists;

    public $id;
    public $name;
    public $mail;
    public $rank;
    private $password;
    private $token;
    
    function __construct(string $name) {
        $this->db = App::getDatabase();

        $user = $this->db->select('SELECT * FROM cre_users WHERE name=:name OR mail=:name', array('name' => $name));

        if(count($user) == 1) {
            $this->exists = true;
            $this->id = $user[0]->id;
            $this->name = $user[0]->name;
            $this->mail = $user[0]->mail;
            $this->rank = $user[0]->rank;
            $this->password = $user[0]->password;
            $this->token = $user[0]->token;
        } else {
            $this->exists = false;
        }
    }

    function exists() {
        return $this->exists;
    }

    function refreshToken() {
        /** @noinspection PhpUnhandledExceptionInspection */
        $token = bin2hex(random_bytes(30))."#".$this->mail;
        $dataArray = array('token' => hash("sha256", $token));
        $where = array('mail' => $this->mail);
        $this->db->update('cre_users', $dataArray, $where);
        $this->token = hash("sha256", $token);
        setcookie('user', $token, time()+60*60*24*365, '/');
        return $token;
    }

    function verifyToken($token) {
        return $token == $this->token;
    }

    function verifyPassword($password) {
        return password_verify($password, $this->password);
    }

    function logout() {
        $this->refreshToken();
        Session::getInstance()->unset('user');
        setcookie('user', '', time()-3600, '/');
        setcookie('rememberme', '', time()-3600, '/');
    }

    static function login($mail, $password, $remember) {
        $user = new User($mail);
        if($user->exists()) {
            if($user->verifyPassword($password)) {
                $user->refreshToken();
                setcookie('rememberme', (isset($remember) && $remember == "remember-me") ? 'true' : 'false', time()+60*60*24*365, '/');
                $user->writeSession();
                return true;
            }
        }

        return false;
    }

    static function register($name, $mail, $password) {
        $hashed = password_hash($password, PASSWORD_ARGON2ID, array('memory_cost' => 4096,'time_cost' => 5, 'threads' => 8));
        $data = array('name' => $name, 'mail' => $mail, 'password' => $hashed);
        App::getDatabase()->insert("cre_users", $data);
        
        $user = new User($mail);
        $user->refreshToken();
        $user->writeSession();
        setcookie('rememberme', (isset($remember) && $remember == "remember-me") ? 'true' : 'false', time()+60*60*24*365, '/');
    }

    static function remember($cookie) {
        $mail = explode("#", $cookie)[1];
        $user = new User($mail);

        if($user->exists()) {
            if($user->verifyToken(hash("sha256", $cookie))) {
                $user->refreshToken();
                $user->writeSession();
                return true;
            }
        }

        return false;
    }

    /**
     * Check if user is logged, returns the User object if true, else null
     * @return mixed User object or null
     */
    static function logged() {
        if(Session::getInstance()->read('user') != null)
            return new User(Session::getInstance()->read('user')['name']);
        else
            return null;
    }

    private function writeSession() {
        Session::getInstance()->write('user', array(
            'name' => $this->name,
            'mail' => $this->mail,
            'rank'=> $this->rank
        ));
    }
}