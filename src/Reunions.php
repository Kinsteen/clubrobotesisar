<?php

class Reunions
{

    private $db;

    public function __construct(Database $db) {
        $this->db = $db;
    }

    public function all() {
        return $this->db->select('SELECT * FROM cre_reunions');
    }

    public function get(int $id) {
        $res = $this->db->select('SELECT * FROM cre_reunions WHERE id=:id', array('id' => $id));

        if (count($res) == 0) {
            return null;
        } else {
            return $res[0];
        }
    }

    public function count() {
        return sizeof($this->all());
    }

    public function ghost() {
        $ghost = $this->get(1);
        return $ghost;
    }

    public function last() {
        $all = $this->all();

        if ($this->count() > 1) {
            $lastreunionid = 0;
            $lastreuniontime = strtotime(str_replace('/', '-', $all[1]->date));
            foreach($all as $key => $reunion) { // Cycle through every reunions, compare 1v1 to find the last
                if($reunion->id != 1) { // Exclude ghost reunion
                    $date = str_replace('/', '-', $reunion->date);
                    if($lastreuniontime <= strtotime($date)) {
                        $lastreunionid = $key;
                        $lastreuniontime = strtotime($date);
                    }
                }
            }

            return $all[$lastreunionid];
        } else {
            return null;
        }
    }

    public function editGhost(string $date, string $orders): string {
        $q = $this->db->select('SELECT id FROM cre_reunions WHERE date=:date AND id != 1', array('date' => $date));
        if(sizeof($q) > 0) {
            return '-1';
        }

        $this->db->update('cre_reunions', array('date' => $date, 'orders' => $orders), array('id' => 1));
        return '1';
    }

}
