<?php

/**
 * 
 */
class Util {
    
    public static function compare($a, $b) {
        if(isset($a) && isset($b) && !empty($a) && !empty($b) && $a === $b)
            return true;
        return false;
    }

    public static function filesize_formatted($size) {
        $units = array( 'o', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1024, $power), 2, ',', ' ') . ' ' . $units[$power];
    }

    /**
     * Search if string starts with a specified string
     * Can pass array of strings in needle
     * 
     * @param  string Where to search
     * @param  mixed What to search
     * @return boolean Match
     */
    public static function startsWith($haystack, $needle) {
        if(gettype($needle) == 'array') {
            $doesstart = false;

            foreach ($needle as $element) {
                substr($haystack, 0, strlen($element)) != $element ?: $doesstart = true;
            }       

            return $doesstart;
        } else {
            return substr($haystack, 0, strlen($needle)) == $needle;
        }

    }

}