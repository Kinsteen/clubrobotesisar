<?php

use Middlewares\AuthMiddleware;
use Middlewares\CSRFMiddleware;
use Middlewares\ModeMiddleware;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Uri;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Twig\Extension\DebugExtension;
use \Psr\Container\ContainerInterface;
use Twig\TwigFunction;

/*
 * This project will be the same as V1, but updated with a better framework and template engine (Slim/Twig)
 * If I find the use, I'll maybe use the M in MVC, but for now it is a complete View/Controller project.
 */

require_once '../vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$app = new \Slim\App(['settings' => [
    'displayErrorDetails' => true,
    'addContentLengthHeader' => false // If true, response is cut
]]);

// Fetch DI Container
$container = $app->getContainer();

unset($container['notFoundHandler']); // Redefine error 404 page
$container['notFoundHandler'] = function ($c) {
    return function () use ($c) {
        $response = new Response(404);
        return $c['view']->render($response, 'error/404.twig');
    };
};

// Register Twig View helper
$container['view'] = function (ContainerInterface $c) : Twig {
    $view = new Twig('../views', [
        'debug' => true,
        'cache' => '../tmp/cache'
    ]);

    $view->getEnvironment()->addFunction(new TwigFunction('generateCSRF', function () {
        return App::generateCSRF();
    }));
    
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = Uri::createFromEnvironment(new Environment($_SERVER));
    $view->addExtension(new TwigExtension($router, $uri));
    $view->addExtension(new DebugExtension());

    return $view;
};

$app->add(function(Request $request, Response $response, callable $next) {
    $this->get('view')->getEnvironment()->addGlobal("theme", isset($_COOKIE['theme']) ? $_COOKIE['theme'] : 'light');
    $this->get('view')->getEnvironment()->addGlobal("user", User::logged());
    $this->get('view')->getEnvironment()->addGlobal("flashes", Session::getInstance()->getFlashes()); // Pass globally flashes to view, so we do not have to pass it everytime
    return $next($request, $response);
})->add(new ModeMiddleware($container));

$app->get('/', '\Controllers\HomeController:home')->add('\Middlewares\MiscMiddleware:remember'); // This will first instanciate HomeController, and pass the container as first argument

$app->get('/inventaire', '\Controllers\InventoryController:index')->add(new AuthMiddleware())->add('\Middlewares\MiscMiddleware:remember');

$app->get('/liens', '\Controllers\HomeController:links')->add('\Middlewares\MiscMiddleware:remember');

$app->group('/admin', function() use ($app) {
    $app->get('', '\Controllers\AdminController:index');
    $app->post('/rank-account', '\Controllers\AdminController:rank')->add(new CSRFMiddleware());
})->add(new AuthMiddleware(2))->add('\Middlewares\MiscMiddleware:remember');

$app->get('/accounts', '\Controllers\AccountsController:index')->add(new AuthMiddleware())->add('\Middlewares\MiscMiddleware:remember');

$app->group('/accounts', function() use ($app) {
    $app->get('/login', '\Controllers\AccountsController:showLogin')->setName('login');
    $app->post('/login', '\Controllers\AccountsController:login');

    $app->get('/register', '\Controllers\AccountsController:showRegister');
    $app->post('/register', '\Controllers\AccountsController:register');
})->add('\Middlewares\MiscMiddleware:guest');

$app->get('/accounts/logout', '\Controllers\AccountsController:logout')->add(new AuthMiddleware());

$app->group('/reunions', function() use ($app) {
    $app->get('', '\Controllers\ReunionsController:index');
    $app->post('', '\Controllers\ReunionsController:editGhost')->add(new CSRFMiddleware())->add(new AuthMiddleware(2)); // Changes date and orders of ghost reunion (cf ReunionsController)
    $app->get('/list', '\Controllers\ReunionsController:showList');
    $app->get('/show/{id}', '\Controllers\ReunionsController:show');
    $app->get('/edit/{id}', '\Controllers\ReunionsController:showEdit')->add(new AuthMiddleware(2));
    $app->post('/edit', '\Controllers\ReunionsController:postEdit')->add(new CSRFMiddleware())->add(new AuthMiddleware(2));
    $app->get('/add', '\Controllers\ReunionsController:showAdd')->add(new AuthMiddleware(2));
    $app->post('/add', '\Controllers\ReunionsController:postAdd')->add(new CSRFMiddleware())->add(new AuthMiddleware(2));
    $app->post('/delete', '\Controllers\ReunionsController:delete')->add(new CSRFMiddleware())->add(new AuthMiddleware(2));
})->add(new AuthMiddleware())->add('\Middlewares\MiscMiddleware:remember');

$app->group('/articles', function() use ($app) {
    $app->get('', '\Controllers\ArticlesController:index');
    $app->get('/add', '\Controllers\ArticlesController:showAdd');
    $app->post('/add', '\Controllers\ArticlesController:add');
    $app->get('/edit', '\Controllers\ArticlesController:showEdit');
    $app->post('/edit', '\Controllers\ArticlesController:edit');
});

$app->post('/api/update-history', '\Controllers\APIController:updateHistory')->add(new CSRFMiddleware())->add(new AuthMiddleware());
$app->post('/api/clear-history', '\Controllers\APIController:clearHistory')->add(new CSRFMiddleware())->add(new AuthMiddleware());
$app->post('/api/change-mode', '\Controllers\APIController:changeMode')->add(new CSRFMiddleware())->add(new AuthMiddleware(3));

$app->get('/test', function(Request $request, Response $response) {
    $response->write('<pre>Allo comment ça va</pre>');
    return $response;
});

$app->run();
