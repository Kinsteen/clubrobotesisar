$(function() {
    $(".card-dismiss").on("click", function(e) {
        e.preventDefault();
        
        $.post("api/update-history",
        {
            id: $(this).attr("id")
        }).done(function(data) {
            if(data === '-1') {
                $.notify({
                    message: "Une erreur d'authentification est survenue. Veuillez vous reconnecter. Redirection vers la page de connexion dans 5 secondes..."
                },{
                    type: 'danger',
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                });

                setTimeout(function() {window.location.replace("/accounts/login")}, 5000);
            }
        });

        let target = $(this).closest(".card-container")[0];
        let flip = new Flip();
        flip.read($('.card-container'));
        flip.remove([target]);
        flip.play($('.card-container').not(target));
    });

    $(".col-md-4").on("animationend", function(e) {
        $(this).hide();
    });
});


class Flip {
    constructor() {
        this.duration = 500;
        this.positions = {};
    }

    read(elements) {
        let cl = this;
        elements.each(function(index, element) {
            const id = element.getAttribute('id');
            cl.positions[id] = element.getBoundingClientRect();
        });
    }

    play(elements) {
        let cl = this;
        elements.each(function(index, element) {
            const id = element.getAttribute('id');
            const newPosition = element.getBoundingClientRect()
            const oldPosition = cl.positions[id];
            const deltaX = oldPosition.x - newPosition.x;
            const deltaY = oldPosition.y - newPosition.y;
            const deltaW = oldPosition.width / newPosition.width;
            const deltaH = oldPosition.height / newPosition.height;
            element.animate([{
                transform: `translate(${deltaX}px, ${deltaY}px) scale(${deltaW}, ${deltaH})`
            },{
                transform: 'none'
            }], {
                duration: cl.duration,
                easing: 'ease-in-out',
                fill: 'both'
            })
            element.style.transform = `translate(${deltaX}px, ${deltaY}px) scale(${deltaW}, ${deltaH})`;
        });
    }

    remove(elements) {
        elements.forEach(element => element.parentNode.appendChild(element));
        elements.forEach(element => {
            const id = element.getAttribute('id');
            const newPosition = element.getBoundingClientRect()
            const oldPosition = this.positions[id];
            const deltaX = oldPosition.x - newPosition.x;
            const deltaY = oldPosition.y - newPosition.y;

            element.animate([{
                transform: `translate(${deltaX}px, ${deltaY}px)`,
                opacity: 1

            },{
                transform: `translate(${deltaX-300}px, ${deltaY}px)`,
                opacity: 0
            }], {
                duration: this.duration,
                easing: 'ease-in-out',
                fill: 'both'
            });

            window.setTimeout(function() {
                $(element).remove();
            }, this.duration);
        });
    }
}