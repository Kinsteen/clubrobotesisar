// this file will only handle ajax calls as it is the same as edit-common.js

$(function () {
    $('body').on('save', function (e, date, olddate = null) {
        let orders = [];
        let content = [];

        let $temporders = $('ul#order > li:not(.add-order):not(.ghost)').clone().children().remove().end(); // do not take .ghost into account, because it's element to be removed cause of fadeOut
        let $tempcontent = $('div:not(.ghost) > div > div > textarea');

        $temporders.each(function (index, element) {
            orders.push($(element).text());
        });

        $tempcontent.each(function (index, element) {
            content.push($(element).val());
        });

        if(olddate == null) {
            $.post('/reunions/edit', {changedate: false, date: date, orders: orders, content: content}).done(function (data) {
                // Nothing to do here
            });
        } else {
            $.post('/reunions/edit', {changedate: true, olddate: olddate, date: date, orders: orders, content: content}).done(function (data) {
                if(data === "-1") {
                    $.notify({
                        message: "Vous ne pouvez pas modifier la date, une réunion existe déjà pour cette date."
                    },{
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                    });
                }
            });
        }

        // Ajax save this shit
    });
});