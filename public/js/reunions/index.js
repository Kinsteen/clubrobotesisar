$(function() {
    $.fn.datepicker.setDefaults({
        format: 'dd/mm/yyyy',
        days: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        daysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        daysMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        weekStart: 1,
        months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
        startDate: 'today'
    });

    setupDatePicker();

    $('#order > li:not(.add-order)').hover(function() {
        $(this).children('.delete-button').remove();
        $('<button class="delete-button" style="display:none"><i class="fas fa-times"></i></button>').appendTo(this).fadeIn('fast').on('click', function() {
            $(this).closest('li').fadeOut(function () {
                $(this).remove();
                saveGhost();
            });
        });
    }, function() {
        $(this).children('button').fadeOut();
    });

    bindNewOrder();
});

function bindNewOrder() {
    $(".add-order > .text-input").keypress(function(e) {
        let neworder = $(this).val();
        if(e.which === 13 && neworder !== '') {
            let ulparent = $(this).closest('ul');
            $(this).parent('li').remove();
            $('<li>' + neworder + '</li>').appendTo(ulparent).hover(function() {
                $(this).children('.delete-button').remove();
                $('<button class="delete-button" style="display:none"><i class="fas fa-times"></i></button>').appendTo(this).fadeIn().on('click', function() {
                    $(this).closest('li').fadeOut(function () {
                        $(this).remove();
                        saveGhost();
                    });
                });
            }, function() {
                $(this).children('button').fadeOut();
            }); // No sanitization, need to protect server side. This is a XSS breach, but only for the user who typed it.
            ulparent.append('<li class="add-order"><input type="text" class="text-input" placeholder="Rajouter des éléments..."/></li>');
            $('.text-input').focus();

            // AJAX add
            saveGhost();

            bindNewOrder();
        }
    });
}

function saveGhost(date = $('[data-toggle="datepicker"]').text()) {
    let orders = [];
    let $temporders = $('ul#order > li:not(.add-order)').clone().children().remove().end();

    $temporders.each(function (index, element) {
        orders.push($(element).text());
    });

    $.post('/reunions', {date: date, orders: orders}).done(function(data) {
        if(data === "-1") {
            $.notify({
                message: "Vous ne pouvez pas modifier la date, une réunion existe déjà pour cette date."
            },{
                type: 'danger',
                placement: {
                    from: "top",
                    align: "right"
                },
            });
        }
    });
}

function setupDatePicker() {
    let datepicker = $('[data-toggle="datepicker"]');

    datepicker.on('click', function(e) { e.preventDefault() });

    datepicker.datepicker({
        language: 'fr-FR'
    });

    datepicker.on('pick.datepicker', function (e) {
        $(this).datepicker('hide');
        if($(this).attr('id') === 'pre') {
            $('div.preface > p').html('La prochaine réunion est programmée pour le <strong><a data-toggle="datepicker" href="#">' + e.date.toLocaleDateString() + '</a></strong>.');
            $(this).remove();
            setupDatePicker();
        } else {

        }
        saveGhost(e.date.toLocaleDateString());
    });
}