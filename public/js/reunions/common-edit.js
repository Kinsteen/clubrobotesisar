let hash = 0;
let timeout;

$(function() {
    let cards = $('.collapse-card');
    if(cards.hasClass('ghost')) {
        hash = 0;
    } else {
        hash = cards.length;
    }

    $('span#save').hide();
    $.fn.datepicker.setDefaults({
        format: 'dd/mm/yyyy',
        days: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        daysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        daysMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        weekStart: 1,
        months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
        startDate: 'today'
    });

    let datepicker = $('[data-toggle="datepicker"]');

    //datepicker.on('click', function(e) { e.preventDefault() });

    datepicker.datepicker({
        language: 'fr-FR'
    });

    datepicker.on('pick.datepicker', function (e) {
        // Ajax change date
        save(e.date.toLocaleDateString(), $('[data-toggle="datepicker"]').text());
    });

    $('#order > li:not(.add-order)').hover(function() {
        $(this).children('.delete-button').remove();
        $('<button class="delete-button" style="display:none"><i class="fas fa-times"></i></button>').appendTo(this).fadeIn('fast').on('click', function() {
            deleteCard(this);
        });
    }, function() {
        $(this).children('button').fadeOut();
    });

    /*$('#quit').on('click', function (e) {
        save();
    });*/

    bindNewOrder();

    hookPosition();

    hookSave();
});

function bindNewOrder() {
    $(this).off();
    $(".add-order > .text-input").keypress(function(e) {
        let neworder = $(this).val();
        if(e.which === 13 && neworder !== '') {
            // Add big div for better animation (no scroll)
            let placeholder = $('<div class="placeholder" style="height: 100vh"></div>');
            $('footer').append(placeholder);
            setTimeout(function () {
                placeholder.remove();
            }, 800);

            let ulparent = $(this).closest('ul');
            $(this).parent('li').remove();
            $('<li>' + neworder + '</li>').appendTo(ulparent).hover(function() {
                $(this).children('.delete-button').remove();
                $('<button class="delete-button" style="display:none"><i class="fas fa-times"></i></button>').appendTo(this).fadeIn().on('click', function() {
                    deleteCard(this);
                });
            }, function() {
                $(this).children('button').fadeOut();
            }); // No sanitization, need to protect server side. This is a XSS breach, but only for the user who typed it.
            ulparent.append('<li class="add-order"><input type="text" class="text-input" placeholder="Rajouter des éléments..."/></li>');

            // Add collapse
            $('.collapse').collapse('hide');
            let $template = $('.template');
            let $newPanel = $template.clone().removeClass('ghost');
            $template.removeClass('template new-collapse').not($newPanel);
            $newPanel.find(".collapse-content").removeClass("show");
            $newPanel.find(".collapse-toggle").attr("data-target",  "#collapse-" + (++hash));
            $newPanel.find(".collapse-toggle").html('<i class="fas fa-chevron-right"></i>' + neworder);
            $newPanel.find(".collapse-toggle").attr("aria-controls",  "collapse-" + (hash));
            $newPanel.attr("id",  "card-" + (hash));
            $newPanel.find("textarea").val("");
            $newPanel.find(".collapse-content").attr("id", "collapse-" + hash).addClass("collapse");
            $("#accordionCR").append($newPanel.fadeIn());

            setTimeout(function() {
                let newcollapse = $('.new-collapse');
                newcollapse.find('.collapse-content').collapse('show');
                newcollapse.find('.form-control').focus();
            }, 500);

            // AJAX add
            save();

            hookPosition();
            hookSave();
            bindNewOrder();

        }
    });
}

function hookPosition() {
    let $up = $('.point-up');
    let $down = $('.point-down');
    $up.closest('a').off('click.position');
    $down.closest('a').off('click.position');
    $up.closest('a').on('click.position', function (e) {
        e.preventDefault();

        let card = $($(this).closest('.collapse-card'));
        let id = parseInt(card.attr('id').substr(5));
        let previouscard = $('#card-' + (id-1));
        if(previouscard.length !== 0) {
            let cards = $('.collapse-card');

            cards.each((i, el) => { // First
                $(el).data('first', el.getBoundingClientRect())
            });

            $(card).insertBefore(previouscard);

            cards.each((i, el) => { // Last
                $(el).data('last', el.getBoundingClientRect())
            });

            cards.each((i, el) => { // Invert/Play
                const $el = $(el);
                const first = $el.data('first');
                const last = $el.data('last');

                const dx = first.left - last.left;
                const dy = first.top - last.top;
                const dw = first.width / last.width;
                const dh = first.height / last.height;

                el.animate([
                    // keyframes
                    { transform: `translate(${dx}px, ${dy}px) scale(${dw}, ${dh})` },
                    { transform: 'translate(0) scale(1)' }
                ], {
                    // timing options
                    duration: 300,
                    easing: "cubic-bezier(0.680, 0, 0.265, 1)"
                });
            });

            $(card).attr('id', 'card-' + (id - 1));
            $(previouscard).attr('id', 'card-' + id);

            $('ul#order > li:eq(' + (id-1) + ')').insertBefore('ul#order > li:eq(' + (id-2) + ')');

            save();
        }
    });

    $down.closest('a').on('click.position', function (e) {
        e.preventDefault();

        let card = $($(this).closest('.collapse-card'));
        let id = parseInt(card.attr('id').substr(5));
        let nextcard = $('#card-' + (id+1));
        if(nextcard.length !== 0) {
            let cards = $('.collapse-card');

            cards.each((i, el) => { // First
                $(el).data('first', el.getBoundingClientRect())
            });

            $(card).insertAfter(nextcard);

            cards.each((i, el) => { // Last
                $(el).data('last', el.getBoundingClientRect())
            });

            cards.each((i, el) => { // Invert/Play
                const $el = $(el);
                const first = $el.data('first');
                const last = $el.data('last');

                const dx = first.left - last.left;
                const dy = first.top - last.top;
                const dw = first.width / last.width;
                const dh = first.height / last.height;

                el.animate([
                    // keyframes
                    { transform: `translate(${dx}px, ${dy}px) scale(${dw}, ${dh})` },
                    { transform: 'translate(0) scale(1)' }
                ], {
                    // timing options
                    duration: 300,
                    easing: "cubic-bezier(0.680, 0, 0.265, 1)"
                });
            });

            $(card).attr('id', 'card-'+ (id+1));
            $(nextcard).attr('id', 'card-' + id);

            $('ul#order > li:eq(' + (id-1) + ')').insertAfter('ul#order > li:eq(' + id + ')');

            save();
        }
    });

}

function hookSave() {
    let textareas = $('textarea');
    textareas.off('input propertychange change.save');
    textareas.on('input propertychange change.save', function() {
        window.onbeforeunload = function(e) {
            save();
            e.returnValue = "Le contenu est en train d'être sauvegardé...";
            return "Le contenu est en train d'être sauvegardé...";
        };
        clearTimeout(timeout);
        timeout = setTimeout(function () { // Save after 1 seconds of inactivity
            window.onbeforeunload = null;
            save();
        }, 1000);
    });
}

/**
 * Fire save event for add.js and edit.js to catch it
 * @param {string} date - The date of the réunion. If date is changed, this is the new date
 * @param {string|null} olddate - Old date only if the date is changed, else null.
 */
function save(date = $('[data-toggle="datepicker"]').text(), olddate = null) {
    if(olddate === null)
        $('body').trigger('save', [date]);
    else
        $('body').trigger('save', [date, olddate]);
    $('span#save').fadeIn(200, function () {
        $(this).fadeOut(2000);
    });
}

function deleteCard(button) {
    let $li = $(button).closest('li');
    let index = $li.index();
    let $card = $('.collapse-card:eq(' + index + ')');
    $li.addClass('ghost');
    $card.addClass('ghost');
    $li.fadeOut(function() {
        $(this).remove();
    });

    $card.fadeOut(function() {
        $(this).remove();
    });

    save();

    //AJAX delete
}
