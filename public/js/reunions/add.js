// this file will only handle ajax calls as it is the same as edit-common.js

$(function () {
    $('#submit').on('click', function () {
        let date = $('[data-toggle="datepicker"]').text();
        let orders = [];
        let content = [];

        let $temporders = $('ul#order > li:not(.add-order):not(.ghost)').clone().children().remove().end();
        let $tempcontent = $('div:not(.ghost) > div > div > textarea');

        $temporders.each(function (index, element) {
            orders.push($(element).text());
        });

        $tempcontent.each(function (index, element) {
            content.push($(element).val());
        });

        $.post('/reunions/add', {date: date, orders: orders, content: content}).done(function(data) {
            if(data === "1") {
                window.location.href ='/reunions/list';
            } else if(data=== '-1') {
                $.notify({
                    message: "Vous ne pouvez pas utiliser cette date, une réunion existe déjà pour cette date."
                },{
                    type: 'danger',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            } else {
                $.notify({
                    message: "Une erreur est survenue. Le contenu n'a pas été sauvegardé. Veuillez le sauver autre part, et prévenir l'administrateur."
                },{
                    type: 'danger',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            }
        });
    });
});