$(function () {
    let urlsplitted = window.location.href.split('/');
    let id = urlsplitted[urlsplitted.length-1];
    console.log(id);

    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        title: 'Êtes-vous sûr ?',
        placement: 'bottom',
        btnOkLabel: 'Oui',
        btnOkClass: 'btn btn-sm btn-success',
        btnCancelLabel: 'Non',
        btnCancelClass: 'btn btn-sm btn-danger',
        onConfirm: function() {
            $.post('/reunions/delete', {id: id}).done(function (data) {
                if(data === '1') {
                    window.location.href = '/reunions/list';
                } else {
                    $.notify({
                        message: "Un problème est survenu. Veuillez contacter l'administrateur."
                    },{
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                    });
                }
            });
        }
    });
});