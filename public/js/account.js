$(function() {
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        title: 'Êtes-vous sûr ?',
        placement: 'bottom',
        btnOkLabel: 'Oui',
        btnOkClass: 'btn btn-sm btn-success',
        btnCancelLabel: 'Non',
        btnCancelClass: 'btn btn-sm btn-danger',
        onConfirm: function() { 
            $(this).addClass("btn-success disabled");
            $(this).removeClass("btn-danger");
    
            $.post("/api/clear-history").done(function(data) {
                if(data === "1") {
                    $.notify({
                        message: "L'historique a bien été supprimé."
                    },{
                        type: 'success',
                        placement: {
                            from: "bottom",
                            align: "center"
                        },
                    });
                } else {
                    $.notify({
                        message: "Une erreur est survenue. Retour à l'accueil dans 5 secondes..."
                    },{
                        type: 'danger',
                        placement: {
                            from: "bottom",
                            align: "center"
                        },
                    });

                    //setTimeout(function() {window.location.replace("/")}, 5000);
                }
            });
            $('[data-toggle=confirmation]').off();
        }
    });

    $('input[name=theme]').on('change', function() {
        let theme = $(this).val();
        if(theme == "dark") {
            $('head').append('<link id="dark-theme-css" rel="stylesheet" type="text/css" href="/css/dark-mode.css">');
        } else {
            $('#dark-theme-css').remove();
        }

        Cookies.set('theme', theme, { expires : 365 });
    });
});