$(function () { // REDO logic, can simplify
    hookRanks();

    $('#maintenance').on('click', function (event) {
        $.post("/api/change-mode", {
            'mode': 'maintenance'
        }).done(function (data) {
            if (data === "maintenance") {
                $('#site-mode').html('Maintenance');
                $.notify({
                    message: "Le site est bien passé en maintenance."
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            } else if (data === "prod") {
                $('#site-mode').html('Prod/dev');
                $.notify({
                    message: "Le site est revenu à la normale (production)."
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            } else if (data === "dev") {
                $('#site-mode').html('Dev');
                $.notify({
                    message: "Le site est revenu à la normale (développement)."
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            } else {
                console.log(data);
            }
        });
    });

    $('#dev-mode').on('click', function (event) {
        $.post("/api/change-mode", {
            'mode': 'dev'
        }).done(function (data) {
            if (data === "dev") {
                $('#site-mode').html('Dev');
                $.notify({
                    message: "Le site est bien passé en dev."
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            } else if (data === "prod") {
                $('#site-mode').html('Prod');
                $.notify({
                    message: "Le site est revenu à la normale."
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            } else {
                console.log(data);
            }
        });
    });
});

function hookRanks() {
    let ranks = ['Membre', 'Membre Actif', 'Bureau', 'Président', 'Développeur'];

    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        title: 'Voulez-vous changer le grade de ce compte ?',
        placement: 'top',
        btnOkLabel: 'Oui',
        btnOkClass: 'btn btn-sm btn-success',
        btnCancelLabel: 'Non',
        btnCancelClass: 'btn btn-sm btn-danger',
        onConfirm: function () {
            let button = $(this);
            let accountName = button.attr('name');
            let trrank = $("[id='" + accountName + "'].account-rank");
            let currentRank = parseInt($("[id='" + accountName + "'].rank-hidden").html());
            let action = button.attr('id') === 'promote-account' ? 'up': 'down';
            let newrank = action === 'up' ? currentRank+1 : currentRank-1;

            $.post("/admin/rank-account",
                {
                    'account': accountName,
                    'rank': currentRank,
                    'action': action}).done(function (data) {
                if(data === '1') {
                    $.notify({
                        message: "Le rang de " + accountName + " a bien été mis à jour."
                    }, {
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                    });

                    let index = trrank.parent('tr').index();

                    $('#user-table').bootstrapTable('updateRow', {
                        index: index,
                        row: {
                            role: ranks[newrank],
                            rankhide: newrank
                        }
                    });

                    $('tr').each(function (index, element) {
                        if($(element).find('.rank-hidden').html() === '0') {
                            $(element).find('#demote-account').prop('disabled', true);
                            console.log($(element).find('#demote-account'));
                        }
                        if($(element).find('.rank-hidden').html() === '1')
                            $(element).find('#demote-account').prop('disabled', false);
                    });

                    hookRanks();
                } else if(data === '0') {
                    $.notify({
                        message: "Vous n'avez pas la permission de changer le rang de cette personne."
                    }, {
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                    });
                } else {
                    $.notify({
                        message: "Il y a eu un problème d'authentification, veuillez réessayer."
                    }, {
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                    });
                }
            });
        }
    });
}