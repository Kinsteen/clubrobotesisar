class Flip {
    static first($el) {
        $el.each((i, el) => { // First
            $(el).data('first', el.getBoundingClientRect())
        });
    }

    static last($el) {
        $el.each((i, el) => { // Last
            $(el).data('last', el.getBoundingClientRect())
        });
    }

    static invert($el) {
        $el.each((i, el) => { // Invert
            const $el = $(el);
            const first = $el.data('first');
            const last = $el.data('last');

            const dx = first.left - last.left;
            const dy = first.top - last.top;
            const dw = first.width - last.width;
            const dh = first.height - last.height;

            $el.css('transform', `translate(${dx}px, ${dy}px) scale(${dw} ${dh})`);
        });
    }

    static play($els) {
        // Don't forget this : const f = document.body.offsetHeight;
        $els.each((i, el) => { // Play
            const $el = $(el);

            $el.addClass('move');
            $el.css('transform', 'translate(0px) scale(0px)');
            $el.on('transitionend', function() {
                $($els).removeClass('move').off('transitionend');
            });
        });
    }
}