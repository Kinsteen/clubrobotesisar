<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Changement de mot de passe - Club Robot Esisar</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Animate CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <!-- Checkbox CSS -->
    <link rel="stylesheet" href="/css/awesome-bootstrap-checkbox.css">

    <!-- Custom styles for this template -->
    <link href="/css/login.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <form class="form-signin" action="" method="post">
            <h2 class="form-signin-heading">Nouveau mot de passe</h2>
            <label for="inputPass1" class="sr-only">Mot de passe</label>
            <input type="password" name="pass1" id="inputPass1" class="form-control" placeholder="Mot de passe"  autofocus>
            <label for="inputPass2" class="sr-only">Mot de passe</label>
            <input type="password" name="pass2" id="inputPass2" class="form-control" placeholder="Confirmez le mot de passe">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Changer le mot de passe</button>
        </form>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="/js/bootstrap-notify.js"></script>
    <?php if(Session::getInstance()->hasFlashes()): ?>
        <?php foreach(Session::getInstance()->getFlashes() as $type => $message): ?>
            <script>
            $(function() {
                $.notify({
                    message: "<?= htmlspecialchars($message); ?>"
                },{
                    type: '<?= $type ?>',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            });
            </script>
        <?php endforeach; ?>
    <?php endif; ?>
</body>
</html>
