<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mot de passe oublié - Club Robot Esisar</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Animate CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <!-- Checkbox CSS -->
    <link rel="stylesheet" href="/css/awesome-bootstrap-checkbox.css">

    <!-- Custom styles for this template -->
    <link href="/css/login.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <form class="form-signin" action="" method="post">
            <a class="btn btn-info btn-lg back-button" href="../" role="button"><i class="fas fa-arrow-left"></i> <span class="back-title">Accueil</span></a>
            <h2 class="form-signin-heading">Mot de passe oublié</h2>
            <label for="inputEmail" class="sr-only">Adresse e-mail du compte</label>
            <input type="email" name="mail" id="inputEmail" class="form-control" placeholder="Adresse e-mail du compte" autofocus style="margin:0;border-radius: 4px">   
            <button class="btn btn-lg btn-primary btn-block" type="submit">Envoyer le mail</button>
        </form>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="/js/bootstrap-notify.js"></script>
    <?php if(Session::getInstance()->hasFlashes()): ?>
        <?php foreach(Session::getInstance()->getFlashes() as $type => $message): ?>
            <script>
            $(function() {
                $.notify({
                    message: "<?= htmlspecialchars($message); ?>"
                },{
                    type: '<?= $type ?>',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            });
            </script>
        <?php endforeach; ?>
    <?php endif; ?>
</body>
</html>
