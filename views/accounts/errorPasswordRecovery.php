<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Kinsteen">

    <link rel="manifest" href="/manifest.json">

    <link rel="icon" href="/img/favicon.ico">

    <title>Erreur 404</title>

    <link rel="canonical" href="https://cre.kinsteen.fr">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> 
    <style type="text/css">
        *{
            transition: all 0.6s;
        }

        body{
            font-family: 'Nunito', 'Lato', sans-serif;
            color: #888;
            margin: 0;
        }

        #main{
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .fof {

        }

        .fof h1{
            font-size: 50px;
            display: inline-block;
            padding-right: 12px;
            animation: type .5s alternate infinite;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        p {
            margin-bottom: 3rem;
        }

        a {
            padding: 14px;
            color: #279CDD;
            border: 1px solid #279CDD;
            border-radius: 5px;
            text-decoration: none;
            cursor: pointer;
        }

        a:hover {
            color: #fff;
            background-color: #279CDD;
        }

        #container {
            display: flex;           /* establish flex container */
            flex-direction: column;  /* make main axis vertical */
            justify-content: center; /* center items vertically, in this case */
            align-items: center;     /* center items horizontally, in this case */
            height: 70vh;
        }

        .box {
            width: 650px;
            margin: 5px;
            text-align: center;     /* will center text in <p>, which is not a flex item */
        }

        @keyframes type{
              from{box-shadow: inset -3px 0px 0px #888;}
              to{box-shadow: inset -3px 0px 0px transparent;}
        }
    </style>
</head>
<body>
    <!--<div id="main">
        <div class="fof">
            <h1>Erreur 404</h1>
            <p>Cette page n'a pas été trouvée.</p>
            <a href="/">Retour à l'accueil</a>
        </div>
    </div>-->

    <div id="container"><!-- flex container -->
        <div class="box fof"><!-- flex item -->
            <h1>Erreur d'authentification</h1>
        </div>
        <div class="box"><!-- flex item -->
            <p>Veuillez recliquer sur le lien du mail.</p>
        </div>
    </div>
</body>
</html>