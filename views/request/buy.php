<?php
$db = App::getDatabase();

App::header('Achat de matériel', 'index', 'demande', true);
?>
  <main role="main">
    <div class="jumbotron" style="border-radius: 0">
      <div class="container">
        <h1 class="display-3">Demande d'achat de matériel</h1>
      </div>
    </div>

    <div class="container">
      <?php if(User::logged()->rank < 3): ?>
      Page en construction...
      <? else: ?>

      <?php endif; ?>
      <hr>
    </div> <!-- /container -->
  </main>

<?php 
App::footer('index');