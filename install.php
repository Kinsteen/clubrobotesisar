<?php

if (!file_exists('config.ini')) {
    echo "Creating config file...\n";
    $content  = "[Database]\r\n";
    $content .= "DB_TYPE = mysql\r\n";
    $content .= "DB_HOST = hosturl\r\n";
    $content .= "DB_NAME = dbname\r\n";
    $content .= "DB_USER = dbuser\r\n";
    $content .= "DB_PASS = dbpassword\r\n";

    file_put_contents('config.ini', $content);

    echo "Please enter db informations in 'config.ini' file, and re-run this script\n";
} else {
    if (!file_exists('app.mode')) {
        echo "Creating app.mode...\n";
        file_put_contents('app.mode', 'dev');
    }

    echo "Connecting to database...\n";
    
    $ini = parse_ini_file('config.ini');

    require "src/Database.php";

    $db = Database::get(array(
        'type' => $ini['DB_TYPE'],
        'host' => $ini['DB_HOST'],
        'name' => $ini['DB_NAME'],
        'user' => $ini['DB_USER'],
        'pass' => $ini['DB_PASS']
    ));

    echo "Connected.\n";
    echo "Creating tables...\n";
    $users  = "CREATE TABLE `cre_users` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) COLLATE utf8_bin NOT NULL,
        `mail` varchar(255) COLLATE utf8_bin NOT NULL,
        `password` varchar(255) COLLATE utf8_bin NOT NULL,
        `rank` int(11),
        `token` varchar(100) COLLATE utf8_bin,
        `seen` text COLLATE utf8_bin,
        `change_password` varchar(100) COLLATE utf8_bin,
        `temp_password` varchar(255) COLLATE utf8_bin,
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

    $articles = "CREATE TABLE `cre_articles` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `author` int(11) NOT NULL,
        `title` text COLLATE utf8_bin NOT NULL,
        `content` text COLLATE utf8_bin NOT NULL,
        `date` varchar(255) COLLATE utf8_bin NOT NULL,
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

    $breaches = "CREATE TABLE `cre_breach` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user` varchar(255) COLLATE utf8_bin NOT NULL,
        `action` text COLLATE utf8_bin NOT NULL,
        `date` varchar(255) COLLATE utf8_bin NOT NULL,
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

    $history = "CREATE TABLE `cre_history` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `title` varchar(255) COLLATE utf8_bin NOT NULL,
        `content` text COLLATE utf8_bin NOT NULL,
        `date` varchar(255) COLLATE utf8_bin NOT NULL,
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

    $reunions = "CREATE TABLE `cre_reunions` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `date` varchar(255) COLLATE utf8_bin,
        `orders` text COLLATE utf8_bin,
        `content` mediumtext COLLATE utf8_bin,
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
    
    $s = $db->prepare("SHOW TABLES LIKE 'cre_users'");
    $s->execute();
    if(count($s->fetchAll()) == 0) {
        $db->query($users);
    }
    $s = $db->prepare("SHOW TABLES LIKE 'cre_articles'");
    $s->execute();
    if(count($s->fetchAll()) == 0) {
        $db->query($articles);
    }
    $s = $db->prepare("SHOW TABLES LIKE 'cre_breach'");
    $s->execute();
    if(count($s->fetchAll()) == 0) {
        $db->query($breaches);
    }
    $s = $db->prepare("SHOW TABLES LIKE 'cre_history'");
    $s->execute();
    if(count($s->fetchAll()) == 0) {
        $db->query($history);
    }
    $s = $db->prepare("SHOW TABLES LIKE 'cre_reunions'");
    $s->execute();
    if(count($s->fetchAll()) == 0) {
        $db->query($reunions);
    }

    echo "Tables created !\n";
    echo "Generating VHost for Apache...\n";
    $vh = "Listen 8888
    
<VirtualHost *:8888>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com

	ServerAdmin webmaster@localhost
	DocumentRoot \"".__DIR__."/public\"

	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn

	ErrorLog \"".__DIR__."/error.log\"
	CustomLog \"".__DIR__."/access.log\" combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with 'a2disconf'.
    #Include conf-available/serve-cgi-bin.conf
    
    <Directory \"".__DIR__."/public\">
        Options -Indexes -MultiViews
        AllowOverride all
        Require all granted
    </Directory>
</VirtualHost>
";
    file_put_contents('cre-default.conf', $vh);
    echo "Generated ! Please copy it to /etc/apache2/sites-available, and run 'a2ensite cre-default'\n";
}